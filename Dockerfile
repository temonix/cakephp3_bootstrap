FROM registry.intranet.aknet.kg:5000/tima/gitlab-runner:php-7.2-ak3

ADD . /app
VOLUME /var/www/html
EXPOSE 9000
ENTRYPOINT ["/app/init.sh"]
